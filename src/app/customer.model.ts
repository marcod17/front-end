export class Customer {
    id: number;
    name: string;
    city: string;
    email: string;
    phone: string;
    age:number;
}