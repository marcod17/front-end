import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// todo importare le classi dal model
import { Customer } from './customer.model'

@Injectable()
export class CustomerService {

    baseUrl = 'http://127.0.0.1:8000/api/customers';

    constructor(private http: HttpClient) {

    }

    // main page
    getCustomers(){
        return this.http.get<Customer[]>(this.baseUrl).toPromise();
        // controllare che magari funzioni solo se messi come stringhe
        // return this.http.get<string[]>(this.baseUrl).toPromise();
    }
    //  single customer from url
    getCustomerFromUrl(url: string): Promise<Customer> {
        return this.http.get<Customer>(url).toPromise();
    }
    // single customer from id
    getCustomerFromId(id: number): Promise<Customer> {
        let url = this.baseUrl + '/' + id + '/';
        return this.getCustomerFromUrl(url);
    }
    
}