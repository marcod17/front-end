import { Component, OnInit, Input } from '@angular/core';
import { Customer } from '../customer.model';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  @Input() customer: Customer = null;
  // specifichiamo che deve prendere gli input di tipo customer e di default dev'essere vuoto
  constructor(public customerService: CustomerService) { }

  ngOnInit() {
  }

}
