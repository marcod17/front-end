import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Customer } from '../customer.model';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  alphabet: any = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  customerList: Customer[] = [];

  currentLetter: string = null;
  // testare se da problemi di tipo o meno
  // dovremme essere in grado in quanto anche nell'esempio con api currentList.results era comunque un array di JSON
  searchText;
  constructor(private customer: CustomerService) { }

  ngOnInit(): void {
    // carichiamo la lista
    this.customer.getCustomers()
      .then(
        // dato che ci da un array di customers assegneremo questo valore a customerList
        p => {
          this.customerList = p.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
          console.log(this.customerList,'customerList');
          // qualora non funzionasse useremo la customerList.legth come parametro per il loop
          // da cui andremo a creare gli oggetti usando la variabile di ciclo come id da richiamare
        }
      )
      .catch(err =>{
        console.log('Fallita la generazione di customerList', err);
      })
    // trasformiamo la stringa in array
    this.alphabet = this.alphabet.split("");
    console.log(this.alphabet);
  }
  // funzione per cambiare la lettera nella rubrica, da utilizzare solo dopo aver ordinato l'array
  checkChangedLetter(string: string){
    if(this.currentLetter == null || string[0] != this.currentLetter){
      this.currentLetter = string[0];
      return true;
    } else {
      return false;
    }
  }
  // funzione per ritornare le iniziali
  initials(value: string){
    var result = "";
    var tokens = value.split(/\s/);
    const forbidden = ['Miss','Mr.','Ms.','Prof.','Dr.']
    for(var i = 0; i < tokens.length; i++){
      if (!this.arrayHas(tokens[i],forbidden)){
        result += tokens[i].substring(0,1).toUpperCase();
      }
      if (result.length > 1){
        break;
      }
    }
    return result;
  }
  // funzione per cercare se ci sono appellativi prima del nome
  arrayHas(toFind, where){
    return (where.indexOf(toFind) > -1);
  }

}
