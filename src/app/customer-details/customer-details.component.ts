import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer.model';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {

  currentCustomer: Customer = null;
  // nextId: number = null;
  // previousId: number = null;
  constructor(private activatedRoute: ActivatedRoute, private customer: CustomerService) { }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(id);
    // facciamo capire al componente che il parametro id deve prenderlo dal link
    if (id != null){
      // se c'è effettivamente un id cerchiamo di farci dare un oggetto attraverso la funzione del service
      this.customer.getCustomerFromId(Number(id))
        .then(
          p => {
            this.currentCustomer = p;
            console.log('currentCustomer id:', id);
            // this.nextId = this.nextCustomer(id);
            // console.log('next customerID', this.nextId);
            // this.previousId = this.previousCustomer(id);
            // console.log(this.previousId);
            // se la promise andrà a buon fine restituirà un oggetto di tipo Customer
            // l'oggetto restituito verrà assegnato a currentCustomer per poterlo utilizzare nel template
          }
        )
    }
  }
  // nextCustomer(number: any) :number{
  //   number = Number(number);
  //   return number + 1;
  // }
  // previousCustomer(number: any) :number{
  //   number = Number(number);
  //   return number - 1;
  // }
}
